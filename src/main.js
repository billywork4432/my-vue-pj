import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import vuetify from "./plugins/vuetify";
import "./plugins";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
import { dom } from "@fortawesome/fontawesome-svg-core";

dom.watch();
Vue.config.productionTip = false;

Vue.component("font-awesome-icon", FontAwesomeIcon);
new Vue({
  router,
  vuetify,
  render: h => h(App)
}).$mount("#app");
